﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using BSA_EF.Services;

namespace BSA_EF.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LINQController : ControllerBase
    {
        LINQService services; 
        public LINQController(LINQService services)
        {
            this.services = services;
        }
        // GET api/LINQ/CountOfUserTasksInProject/{id:int}
        [HttpGet("CountOfUserTasksInProject/{id:int}")]
        public IActionResult GetTask1(int id)
        {
            try
            {
                return Ok(services.GetCountOfUserTasksInProject(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // GET api/LINQ/TasksByUserIdWhereNameLess45/{id:int}
        [HttpGet("TasksByUserIdWhereNameLess45/{id:int}")]
        public IActionResult GetTask2(int id)
        {
            try
            {
                var result = services.GetTasksByUserIdWhereNameLess45(id);
                if (result.Any())
                    return Ok(result);
                else return NotFound("Not found any tasks where name less 45");
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // GET api/LINQ/FinishedTasksInThisYearByUserId/{id:int}
        [HttpGet("FinishedTasksInThisYearByUserId/{id:int}")]
        public IActionResult GetTask3(int id)
        {
            try
            {
                var result = services.GetFinishedTasksInThisYearByUserId(id);
                if (result.Any())
                    return Ok(result);
                else return NotFound("Not found any finished tasks in this year");
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // GET api/LINQ/TeamsMembersOverTenYearsOld
        [HttpGet("TeamsMembersOverTenYearsOld")]
        public IActionResult GetTask4()
        {
            return Ok(services.GetTeamsMembersOverTenYearsOld());
        }

        // GET api/LINQ/SortedUsersWithSortedTasks
        [HttpGet("SortedUsersWithSortedTasks")]
        public IActionResult GetTask5()
        {
            return Ok(services.GetSortedUsersWithSortedTasks());
        }

        // GET api/LINQ/UserInfoById/{id:int}
        [HttpGet("UserInfoById/{id:int}")]
        public IActionResult GetTask6(int id)
        {
            try
            {
                return Ok(services.GetUserInfoById(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // GET api/LINQ/ProjectsInfo
        [HttpGet("ProjectsInfo")]
        public IActionResult GetTask7()
        {
            return Ok(services.GetProjectsInfo());
        }
    }
}
