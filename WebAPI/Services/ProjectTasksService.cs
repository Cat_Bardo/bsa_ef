﻿using BSA_EF.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_EF.Models;

namespace BSA_EF.WebAPI.Services
{
    public class ProjectTasksService
    {
        private IUnitOfWork unitOfWork;

        public ProjectTasksService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public List<ProjectTask> GetProjectsTasks()
        {
            return unitOfWork.ProjectTasks.Get().ToList();
        }

        public ProjectTask GetProjectTask(int id)
        {
            return unitOfWork.ProjectTasks.Get(id);
        }

        public void PostProjectTask(ProjectTask task)
        {
            unitOfWork.ProjectTasks.Create(task);
            unitOfWork.Commit();
        }

        public void PutProjectTask(ProjectTask task)
        {
            unitOfWork.ProjectTasks.Update(task);
            unitOfWork.Commit();
        }

        public void DeleteProjectTask(int id)
        {
            unitOfWork.ProjectTasks.Delete(id);
            unitOfWork.Commit();
        }
    }
}
