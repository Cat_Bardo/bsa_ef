﻿using BSA_EF.Interfaces;
using BSA_EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA_EF.WebAPI.Services
{
    public class UsersService
    {
        private IUnitOfWork unitOfWork;

        public UsersService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public List<User> GetUsers()
        {
            return unitOfWork.Users.Get().ToList();
        }

        public User GetUser(int id)
        {
            return unitOfWork.Users.Get(id);
        }

        public void PostUser(User user)
        {
            unitOfWork.Users.Create(user);
            unitOfWork.Commit();
        }

        public void PutUser(User user)
        {
            unitOfWork.Users.Update(user);
            unitOfWork.Commit();
        }

        public void DeleteUser(int id)
        {
            unitOfWork.Users.Delete(id);
            unitOfWork.Commit();
        }
    }
}
