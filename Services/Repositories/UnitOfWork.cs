﻿using BSA_EF.Interfaces;
using BSA_EF.Models;

namespace BSA_EF.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private SqlServerDbContext _context;
        private IRepository<Project> _projects;
        private IRepository<ProjectTask> _projectTasks;
        private IRepository<Team> _teams;
        private IRepository<User> _users;
        public UnitOfWork(SqlServerDbContext context)
        {
            _context = context;
        }

        public IRepository<Project> Projects
        {
            get
            {
                return _projects ??
                    (_projects = new Repository<Project>(_context));
            }
        }

        public IRepository<ProjectTask> ProjectTasks
        {
            get
            {
                return _projectTasks ??
                    (_projectTasks = new Repository<ProjectTask>(_context));
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                return _teams ??
                    (_teams = new Repository<Team>(_context));
            }
        }

        public IRepository<User> Users
        {
            get
            {
                return _users ??
                    (_users = new Repository<User>(_context));
            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}