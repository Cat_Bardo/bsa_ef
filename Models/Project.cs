﻿using Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Project : IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        public User Author { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }
        public Team Team { get; set; }

        public List<ProjectTask> Tasks { get; set; }

        public Project()
        {
            Team = new Team(TeamId);
            Author = new User(AuthorId);
        }

    }
}
