﻿using Newtonsoft.Json;
using BSA_EF.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BSA_EF.WebAPI
{
    public static class Seeder
    {
        public static void Seed(SqlServerDbContext context)
        {
            context.Database.EnsureCreated();
           // context.Database.OpenConnection();
            if (!context.Projects.Any())
            {
                context.Projects.AddRange(JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("StartData\\Projects.json")));
                   
            }
            if (!context.ProjectTasks.Any())
            {
                context.ProjectTasks.AddRange(JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("StartData\\Tasks.json")));
            }

            if (!context.Teams.Any())
            {
                context.Teams.AddRange(JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("StartData\\Teams.json")));
            }

            if (!context.Users.Any())
            {
                context.Users.AddRange( JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("StartData\\Users.json")));
            }
            context.SaveChanges();
           // context.Database.CloseConnection();
        }

    }
}
