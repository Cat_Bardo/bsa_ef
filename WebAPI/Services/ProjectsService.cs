﻿using BSA_EF.Interfaces;
using BSA_EF.Models;
using System.Collections.Generic;
using System.Linq;


namespace BSA_EF.WebAPI.Services
{
    public class ProjectsService
    {
        private IUnitOfWork unitOfWork;

        public ProjectsService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public List<Project> GetProjects()
        {
            return unitOfWork.Projects.Get().ToList();
        }

        public Project GetProject(int id)
        {
           return unitOfWork.Projects.Get(id);
        }

        public void PostProject(Project project) 
        {
            unitOfWork.Projects.Create(project);
            unitOfWork.Commit();
        }

        public void PutProject(Project project)
        {
            unitOfWork.Projects.Update(project);
            unitOfWork.Commit();
        }

        public void DeleteProject(int id) 
        {
            unitOfWork.Projects.Delete(id);
            unitOfWork.Commit();
        }
    }
}
