﻿using Microsoft.AspNetCore.Mvc;
using BSA_EF.Models;
using System;
using Microsoft.EntityFrameworkCore;
using BSA_EF.WebAPI.Services;

namespace BSA_EF.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private TeamsService service;
        public TeamsController(TeamsService service)
        {
            this.service = service;
        }

        // GET api/Teams
        [HttpGet]
        public IActionResult GetTeams()
        {
            return Ok(service.GetTeams());
        }

        // GET api/Teams/id
        [HttpGet("{id}")]
        public IActionResult GetTeam(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(service.GetTeam(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }


        // POST api/Teams
        [HttpPost]
        public IActionResult Post(Team team)
        {
            if(team == null)
            {
                return BadRequest();
            }
            try
            {
                service.PostTeam(team);
                return Created("api/Teams", team);
            }
            catch(ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateException)
            {
                return BadRequest("Entity with the same Id already exists. Please try again with another Id");
            }
        }

        // DELETE api/Teams/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                service.DeleteTeam(id);
                return NoContent();
            }
            catch(ArgumentException e)
            {
                return NotFound("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }

        // PUT api/Teams
        [HttpPut()]
        public IActionResult Put(Team team)
        {
            if (team == null)
            {
                return BadRequest();
            }
            try
            {
                service.PutTeam(team);
                return Ok(team);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }
    }
}
