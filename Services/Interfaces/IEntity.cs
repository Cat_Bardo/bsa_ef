﻿
namespace BSA_EF.Interfaces
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
