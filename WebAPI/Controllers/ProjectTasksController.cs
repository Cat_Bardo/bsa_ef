﻿using Microsoft.AspNetCore.Mvc;
using BSA_EF.Models;
using System;
using Microsoft.EntityFrameworkCore;
using BSA_EF.WebAPI.Services;

namespace BSA_EF.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTasksController : ControllerBase
    {
        private ProjectTasksService service;
        public ProjectTasksController(ProjectTasksService service)
        {
            this.service = service;
        }

        // GET api/ProjectTasks
        [HttpGet]
        public IActionResult GetProjectTasks()
        {
            return Ok(service.GetProjectsTasks());
        }

        // GET api/ProjectTasks/id
        [HttpGet("{id}")]
        public IActionResult GetProjectTask(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(service.GetProjectTask(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }


        // POST api/ProjectTasks
        [HttpPost]
        public IActionResult Post(ProjectTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }
            try
            {
                service.PostProjectTask(task);
                return Created("api/ProjectTasks", task);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateException)
            {
                return BadRequest("Entity with the same Id already exists. Please try again with another Id");
            }
        }

        // DELETE api/ProjectTasks/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                service.DeleteProjectTask(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }

        // PUT api/ProjectTasks
        [HttpPut()]
        public IActionResult Put(ProjectTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }
            try
            {
                service.PutProjectTask(task);
                return Ok(task);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }

    }
}
