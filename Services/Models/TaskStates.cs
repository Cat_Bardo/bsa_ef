﻿using BSA_EF.Interfaces;
using Newtonsoft.Json;

namespace BSA_EF.Models
{
    public enum TaskStates
    {
        Created = 0, Started = 1, Finished = 2, Canceled = 3
    }
}
