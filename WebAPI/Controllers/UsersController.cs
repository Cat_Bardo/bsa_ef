﻿using Microsoft.AspNetCore.Mvc;
using BSA_EF.Models;
using System;
using Microsoft.EntityFrameworkCore;
using BSA_EF.WebAPI.Services;

namespace BSA_EF.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private UsersService service;
        public UsersController(UsersService service)
        {
            this.service = service;
        }

        // GET api/Users
        [HttpGet]
        public IActionResult GetUsers()
        {
            return Ok(service.GetUsers());
        }

        // GET api/Users/id
        [HttpGet("{id}")]
        public IActionResult GetUsers(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try 
            {
                return Ok(service.GetUser(id)); 
            }
            catch(ArgumentException e) 
            { 
                return NotFound(e.Message); 
            }
            
        }

        // POST api/Users
        [HttpPost]
        public IActionResult Post(User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            try
            {
                service.PostUser(user);
                return Created("api/User", user);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateException)
            {
                return BadRequest("Entity with the same Id already exists. Please try again with another Id");
            }
        }

        // DELETE api/Users/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                service.DeleteUser(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }

        // PUT api/Users
        [HttpPut()]
        public IActionResult Put(User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            try
            {
                service.PutUser(user);
                return Ok(user);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }



    }
}
