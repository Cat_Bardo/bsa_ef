﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using BSA_EF.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BSA_EF.Models
{
    public class Team : IEntity
    {
        [JsonProperty("id")]
        [System.ComponentModel.DataAnnotations.Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        public string Discription { get; set; }
    }
}
