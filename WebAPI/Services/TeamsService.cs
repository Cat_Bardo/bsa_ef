﻿using BSA_EF.Interfaces;
using BSA_EF.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_EF.WebAPI.Services
{
    public class TeamsService
    {
        private IUnitOfWork unitOfWork;

        public TeamsService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public List<Team> GetTeams()
        {
            return unitOfWork.Teams.Get().ToList();
        }

        public Team GetTeam(int id)
        {
            return unitOfWork.Teams.Get(id);
        }

        public void PostTeam(Team team)
        {
            unitOfWork.Teams.Create(team);
            unitOfWork.Commit();
        }

        public void PutTeam(Team team)
        {
            unitOfWork.Teams.Update(team);
            unitOfWork.Commit();
        }

        public void DeleteTeam(int id)
        {
            unitOfWork.Teams.Delete(id);
            unitOfWork.Commit();
        }
    }
}
