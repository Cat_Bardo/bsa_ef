﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common.DTOs.LinqDTOs;
using BSA_EF.Models;
using BSA_EF.Interfaces;
using AutoMapper;
using Common.DTOs;

namespace BSA_EF.Services
{
    public class LINQService
    {
        private IUnitOfWork unitOfWork;
        private Mapper mapperUser;
        private Mapper mapperTeam;
        private Mapper mapperProject;
        private Mapper mapperProjectTask;




        public LINQService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.mapperTeam = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<Team,TeamDTO>()));
            this.mapperProject = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<Project, ProjectDTO>()));
            this.mapperProjectTask = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<ProjectTask, ProjectTaskDTO>()));
            this.mapperUser = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>()));
        }
        public List<ProjectCountDTO> GetCountOfUserTasksInProject(int id)
        {
            if (unitOfWork.Projects.Get().Any(p => p.AuthorId == id))
            {
                return unitOfWork.Projects.Get().Where(p => p.AuthorId == id).Select(p => new ProjectCountDTO
                {
                    Key = mapperProject.Map<ProjectDTO>(p),
                    Value = unitOfWork.ProjectTasks.Get().Where(t => t.ProjectId == p.Id).Count()
                }
                ).ToList();
            }
            throw new ArgumentException("Not found projects whis same AuthorId");
        }
        public List<ProjectTask> GetTasksByUserIdWhereNameLess45(int id)
        {
            if (unitOfWork.Users.Get().Any(u => u.Id == id))
            {
                if (unitOfWork.ProjectTasks.Get().Any(t => t.PerformerId == id))
                    return unitOfWork.ProjectTasks.Get().Where(t => t.PerformerId == id && t.Name.Length < 45).ToList();
                throw new ArgumentException("Not found any tasks for user");
            }
            throw new ArgumentException("Not found user");
        }

        public List<TaskIdNameDTO> GetFinishedTasksInThisYearByUserId(int id)
        {
            if (unitOfWork.Users.Get().Any(u => u.Id == id))
            {
                if (unitOfWork.ProjectTasks.Get().Any(t => t.PerformerId == id))
                {
                    return unitOfWork.ProjectTasks.Get()
                    .Where(t => t.PerformerId == id
                        && t.State == (int)TaskStates.Finished
                        && t.FinishedAt.Year == DateTime.Now.Year)
                    .Select(t => new TaskIdNameDTO { Id = t.Id, Name = t.Name }).ToList();
                }
                throw new ArgumentException("Not found any tasks for user");
            }
            throw new ArgumentException("Not found user");
        }

        public List<TeamsUsersDTO> GetTeamsMembersOverTenYearsOld()
        {
            return unitOfWork.Teams.Get()
                .GroupJoin(unitOfWork.Users.Get()
                .Where(x => DateTime.Now.Year - x.Birthday.Year > 10)
                .OrderByDescending(x => x.RegisteredAt),
                 t => t.Id,
                 x => x.TeamId,
                 (team, user) => new TeamsUsersDTO { Team = mapperTeam.Map<TeamDTO>(team), Users = mapperUser.Map<List<UserDTO>>(user.Select(u => u).ToList())})
                .Where(x => x.Users.Any()).ToList();
        }

        public List<UsersTasksDTO> GetSortedUsersWithSortedTasks()
        {
            return unitOfWork.Users.Get().GroupJoin(
                unitOfWork.ProjectTasks.Get()
                .OrderByDescending(x => x.Name.Length),
                u => u.Id,
                t => t.PerformerId,
                (user, tasks) => new UsersTasksDTO { User = mapperUser.Map<UserDTO>(user), Tasks = mapperProjectTask.Map<IEnumerable<ProjectTaskDTO>>(tasks) })
                .OrderBy(x => x.User.FirstName).ToList();
        }

        public UserInfoDTO GetUserInfoById(int id)
        {

            if (unitOfWork.Users.Get().Any(u => u.Id == id))
            {
                if (unitOfWork.Projects.Get().Any(p => p.AuthorId == id)
                    && unitOfWork.ProjectTasks.Get().Any(p => p.PerformerId == id)
                    && unitOfWork.Users.Get(id).TeamId != null
                )
                {
                    return unitOfWork.Users.Get().Select(user => new UserInfoDTO
                    {
                        User = mapperUser.Map<UserDTO>(user),

                        LastProject = mapperProject.Map<ProjectDTO>(unitOfWork.Projects.Get()
                        .Where(p => p.AuthorId == user.Id)
                        .FirstOrDefault(x => x.CreatedAt == unitOfWork.Projects.Get()
                            .Where(p => p.AuthorId == user.Id)
                            .Max(x => x.CreatedAt))),

                        TasksCount =  unitOfWork.ProjectTasks.Get().Where(x => x.ProjectId == unitOfWork.Projects.Get()
                        .Where(p => p.AuthorId == user.Id)
                        .FirstOrDefault(x => x.CreatedAt == unitOfWork.Projects.Get()
                            .Where(p => p.AuthorId == user.Id)
                            .Max(x => x.CreatedAt))?.Id).Count(),

                        UnfinishedAndCanceledTasksCount = unitOfWork.ProjectTasks.Get().Where(x => x.PerformerId == user.Id
                        && (x.State == (int)TaskStates.Started || x.State == (int)TaskStates.Created || x.State == (int)TaskStates.Canceled)).ToList().Count(),
                        LongestTask = mapperProjectTask.Map<ProjectTaskDTO>( unitOfWork.ProjectTasks.Get().Where(x => x.PerformerId == user.Id)
                           .First(x => x.FinishedAt - x.CreatedAt ==
                           unitOfWork.ProjectTasks.Get()
                           .Where(x => x.PerformerId == user.Id).Max(x => x.FinishedAt - x.CreatedAt)))

                    }).First(x => x.User.Id == id);
                }
                throw new ArgumentException("Not found projects and tasks");
            }
            throw new ArgumentException("Not found user");
        }
        public List<ProjectInfoDTO> GetProjectsInfo()
        {

            return unitOfWork.Projects.Get().Where(x => unitOfWork.ProjectTasks.Get().Where(t => t.ProjectId == x.Id).Any()).Select(proj => new ProjectInfoDTO
            {
                Project = mapperProject.Map<ProjectDTO>( proj),

                LongestTaskByDicription = mapperProjectTask.Map<ProjectTaskDTO>( unitOfWork.ProjectTasks.Get().Where(x => x.ProjectId == proj.Id)
                          .First(x => x.Description.Length == unitOfWork.ProjectTasks.Get().Where(x => x.ProjectId == proj.Id)
                          .Max(x => x.Description.Length))),

                LessTaskByName = mapperProjectTask.Map<ProjectTaskDTO>( unitOfWork.ProjectTasks.Get().Where(x => x.ProjectId == proj.Id)
                          .First(x => x.Name.Length == unitOfWork.ProjectTasks.Get().Where(x => x.ProjectId == proj.Id)
                          .Min(x => x.Name.Length))),

                UserCounWhereDescriptionMore20orTaskCounLess3 = unitOfWork.Users.Get()
                .Where(u => u.TeamId == unitOfWork.Projects.Get().Where(x => x.Description.Length > 20 ||
                unitOfWork.ProjectTasks.Get()
                .Where(t => t.ProjectId == x.Id).Count() < 3).FirstOrDefault(x => x.Id == proj.Id).Id).Count()


            }).ToList();


        }
    }
}
