﻿using Microsoft.EntityFrameworkCore;
using BSA_EF.Models;

namespace BSA_EF
{
    public class SqlServerDbContext : DbContext
    {
        public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options)
            : base(options)
        { }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
       // public DbSet<TaskStates> TaskStates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<ProjectTask>().HasOne(x => x.Project).WithMany(p => p.ProjectTasks).HasForeignKey(k => k.ProjectId);
           // modelBuilder.Entity<Project>().HasOne(x => x.Author).WithMany(p => p.Projects).HasForeignKey(k => k.AuthorId);
            //modelBuilder.Entity<Project>().HasOne(x => x.Team).WithMany(p => p.Projects).HasForeignKey(k => k.TeamId);
            //modelBuilder.Entity<ProjectTask>().HasOne(x => x.Performer).WithMany(p => p.Tasks).HasForeignKey(k => k.PerformerId);
            //modelBuilder.Entity<User>().HasOne(x => x.Team).WithMany(p => p.Users).HasForeignKey(k => k.TeamId);
        }

    }
}
