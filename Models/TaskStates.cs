﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum TaskStates
    {
        Created = 0, Started = 1, Finished = 2, Canceled = 3
    }
}
