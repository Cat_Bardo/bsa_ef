﻿using System;
using System.Collections.Generic;


namespace BSA_EF.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> Get();
        TEntity Get(int id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
        void Delete(TEntity entity);

    }
}

