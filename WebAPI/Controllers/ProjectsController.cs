﻿using System;
using Microsoft.AspNetCore.Mvc;
using BSA_EF.Models;
using Microsoft.EntityFrameworkCore;
using BSA_EF.WebAPI.Services;

namespace BSA_EF.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private ProjectsService service;

        public ProjectsController(ProjectsService service)
        {
            this.service = service;
        }

        // GET: api/Projects
        [HttpGet]
        public IActionResult GetProjects()
        {
            return Ok(service.GetProjects());
        }


        //GET: api/Projects/id
        [HttpGet("{id}")]
        public IActionResult GetProject(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(service.GetProject(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }

        //POST: api/Projects
        [HttpPost("")]
        public IActionResult PostProject(Project project)
        {
            if (project == null)
            {
                return BadRequest();
            }
            try
            {
                service.PostProject(project);
                return Created("api/Projects", project);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateException) 
            {
                return BadRequest("Entity with the same Id already exists. Please try again with another Id");
            }
        }

        //PUT: api/Projects
        [HttpPut("")]
        public IActionResult PutProject(Project project)
        {
            if (project == null)
            {
                return BadRequest();
            }
            try
            {
                service.PutProject(project);
                return Ok(project);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }

        //DELETE: api/Projects
        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                service.DeleteProject(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound("Entity with this Id hasn't already exists. Please try again with another Id");
            }
        }
    }
}


