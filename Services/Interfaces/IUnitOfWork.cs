﻿using BSA_EF.Models;

namespace BSA_EF.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<ProjectTask> ProjectTasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
        void Commit();
}
}
