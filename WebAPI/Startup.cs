using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BSA_EF.Interfaces;
using BSA_EF.Repositories;
using BSA_EF.Services;
using Microsoft.EntityFrameworkCore;
using BSA_EF.WebAPI.Services;

namespace BSA_EF.WebAPI
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddScoped<LINQService>();
            services.AddScoped<ProjectsService>();
            services.AddScoped<ProjectTasksService>();
            services.AddScoped<TeamsService>();
            services.AddScoped<UsersService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<SqlServerDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("QueryModelsDatabase"))); ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SqlServerDbContext context)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
         
            app.UseRouting();
            Seeder.Seed(context);
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            }); 
            
        }
    }
}
